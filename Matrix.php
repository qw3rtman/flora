<?php

namespace flora;

/**
 * Matrices.
 */
class Matrix
{
	public $dimensions = NULL;
	public $entries = NULL;

	public function __construct($dimensions, $entries = []) {
		$this->set_dimensions($dimensions);
		$this->set_entries($entries);
	}

	private function set_dimensions($dimensions) // Set the dimensions of the matrix. Format: $dimensions = ['rows' => NUMBER_OF_ROWS, 'columns' => NUMBER_OF_COLUMNS].
	{
		if (is_int($dimensions)) {
			$this->dimensions['rows'] = $this->dimensions['columns'] = $dimensions;
		}

		if (is_array($dimensions)) {
			if (isset($dimensions[0]) && isset($dimensions[1])) {
				if (is_int($dimensions[0]) && is_int($dimensions[1])) {
					$this->dimensions['rows'] = $dimensions[0];
					$this->dimensions['columns'] = $dimensions[1];
				}
			}
		}
	}

	private function set_entries($entries) // Set the values of the Matrix. If no value is specified for a certain "cell", 0 is placed as a default. Format: $entries = [1 => [1 => 2, 3 => 4, 5 => 9, etc...], etc...].
	{
		for ($i = 1; $i <= $this->dimensions['rows']; $i++) {
			for ($j = 1; $j <= $this->dimensions['columns']; $j++) {
				if (is_numeric($entries[$i][$j])) {
					$entries[$i][$j] += 0; // Converts from String to Integer/Float.
					$this->entries[$i][$j] = $entries[$i][$j];

					if (!$this->entries[$i][$j]) $this->entries[$i][$j] = 0;
				} else {
					$this->entries[$i][$j] = 0; // Sets to 0 if the input is not a number.
				}
			}
		}
	}

	public function add(Matrix $matrix) // Adds this matrix to the specified ($matrix) matrix.
	{
		if ($this->dimensions == $matrix->dimensions) {
			$sum_entries = [];
			for ($i = 1; $i <= $this->dimensions['rows']; $i++) {
				for ($j = 1; $j <= $this->dimensions['columns']; $j++) {
					$sum_entries[$i][$j] = $this->entries[$i][$j] + $matrix->entries[$i][$j];
				}
			}

			return new Matrix([$this->dimensions['rows'], $this->dimensions['columns']], $sum_entries);
		}

		return false;
	}

	public function subtract(Matrix $matrix) // Subtracts this matrix from the specified ($matrix) matrix.
	{
		if ($this->dimensions == $matrix->dimensions) {
			$difference_entries = [];
			for ($i = 1; $i <= $this->dimensions['rows']; $i++) {
				for ($j = 1; $j <= $this->dimensions['columns']; $j++) {
					$difference_entries[$i][$j] = $this->entries[$i][$j] - $matrix->entries[$i][$j];
				}
			}

			return new Matrix([$this->dimensions['rows'], $this->dimensions['columns']], $difference_entries);
		}

		return false;
	}

	public function multiply($multiplier) // Multiplies this matrix by a constant (scalar multiplication) or another specified ($multiplier) matrix.
	{
		if (is_numeric($multiplier)) { // Scalar multiplication...
			$multiplier += 0; // Converts from String to Integer/Float.

			$product_entries = [];
			for ($i = 1; $i <= $this->dimensions['rows']; $i++) {
				for ($j = 1; $j <= $this->dimensions['columns']; $j++) {
					$product_entries[$i][$j] = $this->entries[$i][$j] * $multiplier;
				}
			}

			return new Matrix([$this->dimensions['rows'], $this->dimensions['columns']], $product_entries);
		} else if ($multiplier instanceof self) { // Matrix multiplication...
			if ($this->dimensions['columns'] == $multiplier->dimensions['rows']) { // Are two matrices compatible?
				$product_entries = [];
				for ($i = 1; $i <= $this->dimensions['rows']; $i++) {
					for ($j = 1; $j < $multiplier->dimensions['columns']; $j++) {
						/**$product_entries[1][1] = ($this->entries[1][1] * $multiplier->entries[1][1]) + ($this->entries[1][2] * $multiplier->entries[2][1]);
						$product_entries[1][2] = ($this->entries[1][1] * $multiplier->entries[1][2]) + ($this->entries[1][2] * $multiplier->entries[2][2]);
						$product_entries[1][3] = ($this->entries[1][1] * $multiplier->entries[1][3]) + ($this->entries[1][2] * $multiplier->entries[2][3]);

						$product_entries[2][1] = ($this->entries[2][1] * $multiplier->entries[1][1]) + ($this->entries[2][2] * $multiplier->entries[2][1]);
						$product_entries[2][2] = ($this->entries[2][1] * $multiplier->entries[1][2]) + ($this->entries[2][2] * $multiplier->entries[2][2]);
						$product_entries[2][3] = ($this->entries[2][1] * $multiplier->entries[1][3]) + ($this->entries[2][2] * $multiplier->entries[2][3]);**/

						$product_entries[$i][$j] = ($this->entries[$i][$i] * $multiplier->entries[$i][$j]) + ($this->entries[$i][$i + 1] * $multiplier->entries[$i + 1][$j]);
					}
				}

				return new Matrix([$this->dimensions['rows'], $multiplier->dimensions['columns']], $product_entries);
			}
		}

		return false;
	}

	public function inverse() // Returns the inverse of this matrix.
	{

	}

	public function determinant() // Returns the determinant of this matrix.
	{

	}

	public function transpose() // Returns the transposition of this matrix.
	{

	}
}

$entries = [
	1 => [1 => 0, 2 => 1, 3 => 2],
	2 => [1 => 9, 2 => 8, 3 => 7]
];

$A = new Matrix([2, 3], $entries);

$entries = [
	1 => [1 => 6, 2 => 5, 3 => 4],
	2 => [1 => 3, 2 => 4, 3 => 5]
];

$B = new Matrix([2, 3], $entries);

$entries = [
	1 => [1 => 1, 2 => 2],
	2 => [1 => 3, 2 => 4]
];

$X = new Matrix([2, 2], $entries);

$entries = [
	1 => [1 => 5, 2 => 6, 3 => 7],
	2 => [1 => 8, 2 => 9, 3 => 10]
];

$Y = new Matrix([2, 3], $entries);

$C = $A->add($B);

print_r($C);
