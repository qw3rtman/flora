<?php

namespace flora;

/**
 * Sneaky Fibonacci.
 */
class Fibonacci
{
	public static function is_valid($check) // Determines if number provided is a member of the Fibonacci sequence.
	{

	}

	public static function get_previous($before_this) // Gets previous term in Fibonacci sequence using the input ($before_this) as a starting point.
	{

	}

	public static function get_next($after_this) // Gets following term in Fibonacci sequence using the input ($after_this) as a starting point.
	{

	}

	public static function get_at($location) // Gets term in Fibonacci sequence at a certain specified ($location) index.
	{

	}

	public static function get_until($stop_at) // Generates Fibonacci sequence for a specified number of terms ($stop_at).
	{

	}
}
