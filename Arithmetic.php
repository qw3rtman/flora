<?php

namespace flora;

/**
 * Some usless Arithmetic methods.
 *
 * Seriously, don't use these unless you're looking for extra overhead to spice your code up.
 */
class Arithmetic
{
	public static function add($augend, $addend) // Adds two inputs together.
	{
		return $augend + $addend;
	}

	public static function subtract($minuend, $subtrahend) // Subtracts one input from the other input.
	{
		return $minuend - $subtrahend;
	}

	public static function multiply($multiplicand, $multiplier) // Multiplies one input with the other input.
	{
		return $multiplicand * $multiplier;
	}

	public static function divide($dividend, $divisor) // Divides one input from the other input.
	{
		return $dividend / $divisor;
	}

	public static function exponent($base, $exponent) // Raises one input to a specified exponent.
	{
		return pow($base, $exponent);
	}
}
